<?php require_once "layout/header.php"; ?>
<div id="create" >
	<aside id="message"></aside>
	
	<h3>Main question:</h3>
	<input type="text" class="form-control input-lg pol-quest">
	<h3>Answer:</h3>
	<input type="text" class="form-control input-lg pol-ans"><br>
	<input type="button" value="Add answer" id="adder"><br><br>
	<input type="button" value="Create Poll" id="done">
</div>
<?php require_once "layout/footer.php"; ?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#adder").click(function(){
			$("<input type='text' class='form-control input-lg pol-ans'><br>").insertBefore($("#adder"))
			
		})
		$("#done").click(function(){
			var answers = []
			
			var quest = $('.pol-quest').val()

			$('.pol-ans').each(function(){
				if($(this).val() != ''){
					answers.push($(this).val())
				}
			})

			$.ajax({
				async:false,
				type:"POST",
				url:'ajax.php',
				data:{'answers':answers,'quest':quest},
				success:function(r){
					
					if(r==0){
						$("#message").empty()
						$("#message").prepend("<hr id='gt1'>")
						$("#message").prepend("<h1>Please fill all fields correctly !</h1>")
					}else if(r==1){
						$("#message").empty()
						$("#message").prepend("<hr id='gt1'>")
						$("#message").prepend("<h1>Your poll has been created !</h1>")
					}
					
				}

			})
		})


	})
</script>