<?php require_once "layout/header.php"; ?>
<div id="table-div">
	<table id="cucak">
		<tr>
			<th>#</th>
			<th>Poll's Name</th>
			<th></th>
		</tr>
	</table>
</div>
<hr>
<div id="query">
	<div id="query2" style="text-align: center;">

	</div>
	<fieldset id="checkArray">

		
	</fieldset>

</div>
<div id="graphic">

</div>
<?php require_once "layout/footer.php"; ?>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			async:false,
			type:'POST',
			url:'ajax.php',
			data:{'needTB':'true'},
			dataType:'json',
			success:function(r){
				console.log(r)
				for(i=0;i<r.length;i++){
					var elm = $("<tr></tr>");
					$(elm).append("<td>"+(i+1)+"</td>");
					$(elm).append("<td>"+r[i].quest+"</td>");
					var btn = $("<button class='danger'>Activate</button>");
					$(btn).attr('data-list',r[i].id);
					$(btn).attr('data-quest',r[i].quest);
					var th = $("<td></td>");
					$(th).append($(btn));
					$(elm).append($(th));
					$("#cucak").append($(elm));
				}
			}
		})

		$('body').on('click','.danger', function(){
			$("#graphic").css({'display':'none'})
			$('.vote').show()
			$("button").removeClass("success").addClass('danger');
			$(this).attr('class','success');
			var x = $(this).attr("data-list");
			var y = $(this).attr("data-quest");

			$.ajax({
				async:false,
				type:"POST",
				url:"ajax.php",
				data:{'needVal':x},
				dataType:"json",
				success:function(r){
					
					$('#query').fadeIn()
					$("#checkArray").empty()
					$("#query2").empty()
					$("#query2").append("<h1>"+y+"</h1>")
					$("#query2").append("<hr>")
					for(i = 0; i < r.length; i++){
						var elm = $("<input type='checkbox' class='qve'>");
						$(elm).attr('data-answer',r[i].name)
						$("#checkArray").append(elm)
						$("#checkArray").append("<h2>"+r[i].name+"</h2><br><br>")

					}
					$("#checkArray").append("<hr>")
					$("#checkArray").append("<button class='vote success'>Vote</button>")

				}
			})
			$("html, body").animate({ scrollTop: $(document).height() }, 1000);
			

		})

		$('body').on('click','.vote',function(){
			
			if( $('input[class=qve]:checked').length == 0){
				$("#checkArray h1").remove()
				$("#checkArray").append("<h1 style='color:#6440DE'>Please choose one of this answers!</h1>")
			}else if($('input[class=qve]:checked').length > 1){
				$("#checkArray h1").remove()
				$("#checkArray").append("<h1 style='color:#E0A60C'>Only one option can be choosed !</h1>")
			}else{
				$('.vote').hide()
		
				$('#graphic').fadeIn()
				$("#checkArray h1").remove()
				var boxes = $('input[class=qve]:checked').attr('data-answer')
				
				$.ajax({
					async:false,
					type:'POST',
					url:'ajax.php',
					data:{'vote':boxes},
					dataType:'json',
					success:function(r){
						console.log(r)
						var c1 = new Graph()
						var result = []
						for(i=0; i<r.length;i++){
							var obj = {}
							obj.name = r[i].name
							obj.data = [parseInt(r[i].value)]
							result.push(obj)
						}
						c1.setData(result)
						// c1.title("")
						c1.create()
						 $("html, body").animate({ scrollTop: $(document).height() }, 1000);


					}


				})
				
			}

			

			
		})

		
	})
</script>