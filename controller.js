var Graph = function(){

		var data ;
		var vernagir ;
        this.title = function(x){
            vernagir = x
        }
	this.setData = function(x){
     	data = x
     }

	this.create = function(){

		console.log(data)

	Highcharts.chart('graphic', {
	 	chart:{
	 		type:'column'
	 	},
	 	title: {
            text: vernagir,
            x: -20 //center

        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [""]
        },
        yAxis: {
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' votes'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: data
    })
  }
    
}
