<?php 

	session_start();
class Model{
		private $conn;
		private $table;

		function __construct(){
			$this->conn= new mysqli('localhost','root','');
		}

		function set_table($x){
			$this->table = $x;
		}
		function select_database($x){
			$this->conn->select_db($x);
		}

		function findAll(){
		
			return $this->conn->query("SELECT * FROM  $this->table")->fetch_all(MYSQLI_ASSOC);
		}

		function find(Array $x){
			$where = '';
			$last = count($x);
			$i = 0;
			
			foreach($x as $a => $b){
				$where.= "$a='$b'";
				if($i!=$last-1){
					$where.=" and ";
				}  
				$i++;
			}
			
			return  $this->conn->query("SELECT * FROM $this->table WHERE $where")->fetch_all(MYSQLI_ASSOC);
		}

		function insert(Array $x){
			$keys = '(';
			$values = '(';
			$last = count($x);
			$i = 0;
			foreach($x as $a => $b){
				$keys.= "$a";
				$values.= "'$b'";
				if($last == $i+1){
					$keys.= ')';
					$values.= ')';
				}
				if($last != $i+1){
					$keys.= ' , ';
					$values.= ' , ';
				$i++;
				}
		}

		$this->conn->query("INSERT into $this->table $keys values $values");
			
		}
		function delete(Array $x){
			$where = '';
			$last = count($x);
			$i = 0;
			foreach ($x as $key => $value) {
				$where.= "$key='$value' ";
				if($last != $i+1){
					$where.= " and ";
				}
				$i++;
			}
			
			 $this->conn->query("DELETE FROM $this->table where $where");
			
			
		}

		function update($id,Array $x){
			$where = '';
			$last = count($x);
			$i = 0;
			foreach ($x as $key => $value) {
				$where.= "$key='$value' ";
				if($last != $i+1){
					$where.= " , ";
				}
				$i++;
			}
				
			$this->conn->query("UPDATE $this->table set $where where id=$id");
		}

		function get_tables(){
			$a = $this->conn->query("show tables")->fetch_all();
			$b = [];
			for($i = 0; $i<count($a); $i++){
				$b[] = $a[$i][0];
			}
			return $b;

		}

		function get_databases(){
			$a = $this->conn->query("show databases")->fetch_all();
			for($i = 0; $i<count($a); $i++){
				$b[] = $a[$i][0];
			}
			return $b;

		}

	} 

	$test = new Model();
	$test->select_database('poll');
	$checkTables = $test->get_tables();


?>